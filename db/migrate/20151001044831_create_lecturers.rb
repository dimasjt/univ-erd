class CreateLecturers < ActiveRecord::Migration
  def change
    create_table :lecturers do |t|
      t.string :name
      t.string :address
      t.string :gender

      t.timestamps null: false
    end
  end
end
