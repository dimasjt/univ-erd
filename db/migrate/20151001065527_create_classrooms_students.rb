class CreateClassroomsStudents < ActiveRecord::Migration
  def change
    create_table :classrooms_students do |t|
      t.belongs_to :classroom, index: true, foreign_key: true
      t.belongs_to :student, index: true, foreign_key: true
    end
  end
end
