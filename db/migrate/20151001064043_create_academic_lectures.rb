class CreateAcademicLectures < ActiveRecord::Migration
  def change
    create_table :academic_lectures do |t|
      t.string :name
      t.belongs_to :classroom, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
