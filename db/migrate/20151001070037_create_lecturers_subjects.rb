class CreateLecturersSubjects < ActiveRecord::Migration
  def change
    create_table :lecturers_subjects do |t|
      t.belongs_to :lecturer, index: true, foreign_key: true
      t.belongs_to :subject, index: true, foreign_key: true
    end
  end
end
