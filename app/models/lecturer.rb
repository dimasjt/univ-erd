class Lecturer < ActiveRecord::Base
  has_and_belongs_to_many :subjects

  validates :name, :gender, presence: true
end
