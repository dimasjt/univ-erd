class AcademicLecture < ActiveRecord::Base
  belongs_to :classroom
  has_many :subjects

  validates :name, presence: true
end
