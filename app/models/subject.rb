class Subject < ActiveRecord::Base
  belongs_to :academic_lecture
  has_many :scores
  has_and_belongs_to_many :lecturers

  validates :name, presence: true
end
