class Classroom < ActiveRecord::Base
  belongs_to :department
  has_one :academic_lecture
  has_and_belongs_to_many :students

  validates :name, presence: true
end
