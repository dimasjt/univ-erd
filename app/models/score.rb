class Score < ActiveRecord::Base
  belongs_to :student
  belongs_to :subject

  validates :score, presence: true
  validates :score, numericality: true
end
