class Student < ActiveRecord::Base
  has_many :scores
  has_many :subject, through: :scores
  has_and_belongs_to_many :classrooms

  validates :name, :gender, presence: true
end
