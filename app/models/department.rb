class Department < ActiveRecord::Base
  has_many :classrooms

  validates :name, presence: true
end
